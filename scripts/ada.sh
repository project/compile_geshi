#!/bin/sh
#
# Rudimentary script used to compile Ada packages
source_path=$1
sub_folder=$2
output=../${sub_folder}_output.txt

# go to the destination
cd $source_path/$sub_folder

# Reset the output with the start date
echo "compile_geshi:start_time: `date +%s`" >$output

# Compile each file in the source folder
exitcode=0
for source in *.adb
do
	gnatmake `basename $source .adb` >>$output 2>&1
	err=$?
	if test $err -ne 0
	then
		# Hmmm... this is not too good but we only expect one exit code
		$exitcode=$err
	fi
done

# Save the result so the Compile GeSHi module detects it
echo "compile_geshi:exit_code: $exitcode" >>$output
echo "compile_geshi:end_time: `date +%s`" >>$output
mv -f $output ../${sub_folder}_results.txt
