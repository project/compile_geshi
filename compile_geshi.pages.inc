<?php

/**
 * @file
 * Generate the compile_geshi pages and teasers.
 */

/**
 * Load a compile_geshi_result node
 *
 * @param $node
 *    The compile_geshi_result node being loaded.
 */
function _compile_geshi_result_load($node) {
  $result_object = new stdClass;
  $result_object->compile_geshi_result_nid = $node->nid;

  $sql = "SELECT exit_code, start_time, end_time FROM {compile_geshi_results} WHERE result_nid = %d";
  $result = db_query($sql, $node->nid);
  $compile_geshi_result = db_fetch_object($result);
  if ($compile_geshi_result) {
    $result_object->compile_geshi_version = $compile_geshi_result->version;
    $result_object->compile_geshi_build = $compile_geshi_result->build;
    $result_object->compile_geshi_exit_code = $compile_geshi_result->exit_code;
    $result_object->compile_geshi_start_time = $compile_geshi_result->start_time;
    $result_object->compile_geshi_end_time = $compile_geshi_result->end_time;
  }
  else {
    // it doesn't exist?! so use some defaults
    $result_object->compile_geshi_version = 0.1;
    $result_object->compile_geshi_build = 1;
    $result_object->compile_geshi_exit_code = -2;
    $result_object->compile_geshi_start_time = 0;
    $result_object->compile_geshi_end_time = 0;
  }

  return $result_object;
}

/**
 * Generate the form for a compile_geshi_result node type.
 */
function _compile_geshi_result_form(&$node, $form_state) {
  global $user;

  // deleting?
  // (note: if you use the mini module, the 'delete' button is removed and thus undefined here)
  if (isset($form_state['values']['op']) && isset($form_state['values']['delete'])
      && $form_state['values']['op'] == $form_state['values']['delete']) {
    drupal_goto('node/' . $node->nid . '/delete');
  }

  // initialize the form with the default node data
  $form = node_content_form($node, $form_state);

  // compile GeSHi saves the command line result (i.e. the exit() value)
  $form['compile_geshi_geshi_node'] = array(
    '#type' => 'textfield',
    '#title' => t('GeSHi node that was compiled'),
    '#size' => 10,
    '#maxlength' => 10,
    // Note: we do not expect users to create such nodes by hand so there is no auto-complete here
  );
  if (isset($node->compile_geshi_geshi_nid)) {
    $geshi_node = node_load(array('nid' => $node->compile_geshi_geshi_nid));
    $form['compile_geshi_geshi_node']['#default_value'] = $geshi_node->compile_geshi_result_nid;
    $form['compile_geshi_geshi_node']['#description'] = t('Node identifier (nid) which was compiled and generated these results: !node',
      array('!node' => l($geshi_node->title, 'node/' . $geshi_node->compile_geshi_result_nid)));
  }
  else {
    $form['compile_geshi_geshi_node']['#description'] = t('This Compile GeSHi Result node is not yet attached to a source node.');
  }

  $form['compile_geshi_exit_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Compiler exit code'),
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => empty($node->compile_geshi_exit_node) || $node->compile_geshi_exit_code == -2 ? t('no code') : $node->compile_geshi_exit_code,
  );

  $date_format = variable_get('compile_geshi_date_format', 'Y/m/d H:i:s');
  $form['compile_geshi_start_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Date and time when the compilation started'),
    '#description' => t('Enter a Unix date. The current date is @d. Use 0 to remove the date.',
      array('@d' => (empty($node->compile_geshi_start_time) ? 'undefined' : date($date_format, $node->compile_geshi_start_time)))),
    '#size' => 64,
    '#maxlength' => 64,
    '#default_value' => empty($node->compile_geshi_start_time) ? 0 : $node->compile_geshi_start_time,
  );

  $form['compile_geshi_end_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Date and time when the compilation ended'),
    '#description' => t('Enter a Unix date. The current date is @d. Use 0 to remove the date.',
      array('@d' => (empty($node->compile_geshi_end_time) ? 'undefined' : date($date_format, $node->compile_geshi_end_time)))),
    '#size' => 64,
    '#maxlength' => 64,
    '#default_value' => empty($node->compile_geshi_end_time) ? 0 : $node->compile_geshi_end_time,
  );

  return $form;
}

/**
 * Insert a new node (hook_insert())
 */
function _compile_geshi_insert($node, $form_state) {
  $sql = "INSERT INTO {compile_geshi_results} (result_nid, exit_code, start_time, end_time) VALUES (%d, %d, %d, %d)";
  db_query($sql, $node->nid, $node->compile_geshi_exit_code,
    $node->compile_geshi_start_time, $node->compile_geshi_end_time);
}

/**
 * Update a node (hook_update())
 */
function _compile_geshi_update($node, $form_state) {
  $sql = "UPDATE {compile_geshi_results} SET exit_code = %d, start_time = %d, end_time = %d WHERE result_nid = %d";
  db_query($sql, $node->compile_geshi_exit_code,
    $node->compile_geshi_start_time, $node->compile_geshi_end_time,
    $node->nid);
}

/**
 * Delete a compile result node (hook_delete())
 */
function _compile_geshi_delete($node) {
  $sql = "DELETE FROM {compile_geshi_results} WHERE geshi_nid = %d";
  db_query($sql, $node->nid);
}

/**
 * Delete a GeSHi node.
 */
function _compile_geshi_geshi_delete($node) {
  $sql = "SELECT p.project_tid, t.name FROM {compile_geshi_project} p, {term_node} n, {term_data} t"
    . " WHERE n.nid = %d AND p.project_tid = n.tid AND n.tid = t.tid";
  $result = db_query($sql, $node->nid);
  $project = db_fetch_object($result);
  if ($project) {
    drupal_set_message(t('GeSHi node removed from project @name', array('@name' => $project->name)), 'status');
    $sql = "UPDATE {compile_geshi_project} SET modified = 1 WHERE project_tid = %d";
    db_query($sql, $project->project_tid);
  }
}

/**
 * A new GeSHi node was inserted.
 */
function _compile_geshi_geshi_insert($node) {
  // at this time we do exactly the same as the update
  _compile_geshi_geshi_update($node);
}

/**
 * Load a GeSHi node extra data.
 */
function _compile_geshi_geshi_load($node) {
  $sql = "SELECT extraction_error AS compile_geshi_extraction_error, last_extraction AS compile_geshi_last_extraction"
    . " FROM {compile_geshi_node} WHERE geshi_nid = %d";
  $result = db_query($sql, $node->nid);
  return db_fetch_object($result);
}

/**
 * When a GeSHi node is updated, make sure the corresponding project
 * gets recompiled as time allows.
 */
function _compile_geshi_geshi_update(&$node) {
  $sql = "SELECT project_tid FROM {compile_geshi_node} g WHERE g.geshi_nid = %d";
  $result = db_query($sql, $node->nid);
  $geshi_node = db_fetch_object($result);

  $node->compile_geshi_extraction_error = NULL;
  $sql = "SELECT p.status, p.project_tid, t.name FROM {compile_geshi_project} p, {term_data} t, {term_node} n"
    . " WHERE n.nid = %d AND p.project_tid = t.tid AND n.tid = t.tid";
  $result = db_query($sql, $node->nid);


  $row = db_fetch_array($result);
  if ($row) {
    if ($geshi_node) {
      if ($geshi_node->project_tid != $row['project_tid']) {
        $sql = "UPDATE {compile_geshi_node} SET project_tid = %d WHERE g.nid = %d";
        db_query($sql, $row['project_tid'], $node->nid);
      }
    }
    // whether active or not, we mark the project as modified
    $sql = "UPDATE {compile_geshi_project} SET modified = 1 WHERE project_tid = %d";
    db_query($sql, $row['project_tid']);
    if ($row['status'] == 0) {
      drupal_set_message(t('GeSHi node attached to inactive project @name.', array('@name' => $row['name'])), 'warning');
    }
    else {
      drupal_set_message(t('GeSHi node attached to project @name.', array('@name' => $row['name'])), 'status');
    }
  }
  else {
    if ($geshi_node) {
      // we may have been removed from another project
      $sql = "SELECT p.project_tid, t.name FROM {compile_geshi_project} p, {term_data} t WHERE project_tid = %d AND t.tid = %d";
      $result = db_query($sql, $geshi_node->project_tid, $geshi_node->project_tid);
      $row = db_fetch_array($result);
      if ($row) {
        $sql = "UPDATE {compile_geshi_project} SET modified = 1 WHERE project_tid = %d";
        db_query($sql, $geshi_node->project_tid);
        drupal_set_message(t('GeSHi node detached from project @name.', array('@name' => $row['name'])), 'warning');
      }
    }
    if (!$row) {
      drupal_set_message(t('GeSHi node not attached to any project.'), 'warning');
    }
  }
  if (!$geshi_node && $row) {
    // should we always created a compile_geshi_node even if not attached to a project?
    $sql = "INSERT INTO {compile_geshi_node} (geshi_nid, project_tid) VALUES (%d, %d)";
    db_query($sql, $node->nid, $row['project_tid']);
  }
}

/**
 * Add the compilation results to a GeSHi node output view.
 */
function _compile_geshi_geshi_view(&$node, $teaser, $page) {
  // only add our info on complete pages, not teasers
  if ($page) {
    $sql = "SELECT p.*, t.name, t.description FROM {compile_geshi_project} p, {term_node} n, {term_data} t"
      . " WHERE n.nid = %d AND n.tid = t.tid AND p.project_tid = t.tid";
    $result = db_query($sql, $node->nid);
    $project = db_fetch_object($result);
    if ($project) {
      // get the last result which gives us the exit code (if any)
      $sql = "SELECT r.* FROM {compile_geshi_results} r, {term_node} t WHERE r.result_nid = t.nid AND r.result_nid = %d ORDER BY end_time DESC";
      $result = db_query_range($sql, $node->nid, 0, 1);
      $project->last_results = db_result($result);

      // the date format to format Compile GeSHi dates
      $project->date_format = variable_get('compile_geshi_date_format', 'Y/m/d H:i:s');

      // look into how we can override the GeSHi node definition (with $op = view() maybe?)
      $node->content['compile_geshi_info'] = array(
        '#value' => theme('compile_geshi_geshi_node_info', $node, $project),
        '#weight' => variable_get('compile_geshi_geshi_node_info_weight', 10),
      );
    }
  }
}

/**
 * Implement the hook_nodeapi() for GeSHi nodes.
 */
function _compile_geshi_nodeapi(&$node, $op, $teaser, $page) {
  // apply functions to geshinodes
  if ($node->type == 'geshinode') switch ($op) {
  case 'delete':
  case 'insert':
  case 'load':
  case 'update':
  case 'view':
    return call_user_func("_compile_geshi_geshi_$op", $node, $teaser, $page);

  }
}

// vim: ts=2 sw=2 et syntax=php
