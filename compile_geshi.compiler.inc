<?php

/**
 * @file compile_geshi.compiler.inc
 *
 * Prepare, compile and save the results of compilations.
 */

/**
 * Extract then specified number of GeSHi nodes to the
 * development environment.
 *
 * The extraction process works on a per project basis. A project
 * is defined by a taxonomy term marked as the Project taxonomy.
 *
 * (1) Check for results of previous compilations
 *
 * (2) We first extract the oldest source file that was not yet
 *     compiled.
 *
 * (3) We extract any other file that are defined in that one
 *     project and were not extracted yet.
 *
 * (4) If we extracted less than $max_count files, then start
 *     compiling that project, otherwise we consider that we
 *     do not have enough time.
 *
 * (5) If $max_count was not yet reached, continue with the next
 *     project. ($max_count is reduced by the number of files
 *     that were already extracted in process (2) and (3))
 *
 * The function uses an exclusive lock to avoid multiple processes
 * from accessing the database simultaneously and thus prevent
 * starting the compiling the same project twice in a row.
 *
 * @param $max_count
 *    The maximum number of GeSHi nodes to extract in one CRON run.
 *
 * @param $start_compiling
 *    Whether the compilation process should be started.
 */
function _compile_geshi_extract($max_count = 50, $start_compiling = FALSE) {
  // lock for 240 seconds or less
  if ($max_count > 0 && lock_acquire('compile_geshi_extract', 240)) {
    // if the caller authorized compilation on this call, then we check for previous
    // results; this usually happens in hook_cron()
    if ($start_compiling) {
      _compile_geshi_retrieve_results($max_count);
    }
    if ($max_count > 0) {
      if ($start_compiling) {
        // reset the lock
        lock_acquire('compile_geshi_extract', 240);
      }
      _compile_geshi_extract_source_nodes($max_count, $start_compiling);
    }
    lock_release('compile_geshi_extract');
  }
}

/**
 * Fetch the results of the last compilation.
 *
 * This process gets the results file from the compilation environment.
 *
 * @param &$max_count
 *    The maxium number of extractions we will do. Note that in this case
 *    we take the size of the results file in account. Larger files count
 *    as multiple extractions.
 */
function _compile_geshi_retrieve_results(&$max_count) {
  // IMPORTANT NOTE: we ignore the 'active' flag here since we could have started
  //                 a compilation before the project was deactivated and we want
  //                 to retrieve the results either way.
  $sql = "SELECT p.*, t.name FROM {compile_geshi_project} p, {term_data} t WHERE p.compiling = 1 AND p.project_tid = t.tid";
  $result = db_query_range($sql, 0, $max_count);
  while ($max_count > 0 && $project = db_fetch_object($result)) {
    $project_path = _compile_geshi_project_path($project);
    $project_results = $project_path . '_results.txt';
    if (file_exists($project_results)) {
      // retrieve and save the results
      $results = file_get_contents($project_results);
      _compile_geshi_create_results($project, $results, $max_count);
      $project_results_backup = $project_path . '_results.bak';
      rename($project_results, $project_results_backup);

      // we're done compile this project
      $sql = "UPDATE {compile_geshi_project}"
        . " SET compiling = 0, build = build + 1, last_exit_code = %d, last_start_time = %d, last_end_time = %d"
        . " WHERE project_tid = %d";
      db_query($sql, $project->last_exit_code, $project->last_start_time, $project->last_end_time, $project->project_tid);
    }
  }
}

/**
 * Compute the path to the project.
 */
function _compile_geshi_project_path(&$project) {
  // name of the project (i.e. the actual term name)
  $name = drupal_strtolower(str_replace(' ', '-', $project->name));

  // the project version
  $version = drupal_strtolower(str_replace(' ', '_', $project->version));

  // build the path to the folder were we want the source code to reside
  $project->sub_folder = $project->path . '/' . $name . '_' . $version . '-' . $project->build;

  return $project->sub_folder;
}

/**
 * Create a result node and save the compiler output in it.
 */
function _compile_geshi_create_results($project, $results, &$max_count) {
  // extract exit code
  if (preg_match('/compile_geshi:exit_code: (\d+)/', $results, $value) == 1) {
    $project->last_exit_code = $value[1];
  }
  else {
    $project->last_exit_code = 0;
  }

  // extract the start & end times
  if (preg_match('/compile_geshi:start_time: (\d+)/', $results, $value) == 1) {
    $project->last_start_time = $value[1];
  }
  else {
    $project->last_start_time = 0;
  }
  if (preg_match('/compile_geshi:end_time: (\d+)/', $results, $value) == 1) {
    $project->last_end_time = $value[1];
  }
  else {
    $project->last_end_time = 0;
  }

  // make sure the user wants to save some results
  if ($project->result_count < 0) {
    return;
  }

  // each extraction is counted as at least one plus one for each 64Kb we retrieve
  $max_count = max($max_count - 1 - strlen($results) / 65536, 0);

  // some of the functions used to create a node require node pages include
  module_load_include('inc', 'node', 'node.pages');

  // The core defines a way to create a node which is to fill a form
  // and then call drupal_execute() from includes/form.inc.
  $sub_form_state = array();

  // TITLE
  $sub_form_state['values']['title'] = basename($project->sub_folder);

  // BODY
  $sub_form_state['values']['body'] = $results;

  // SPECIFIC RESULTS
  $sub_form_state['values']['compile_geshi_version'] = $project->version;
  $sub_form_state['values']['compile_geshi_build'] = $project->build;
  $sub_form_state['values']['compile_geshi_exit_code'] = $project->last_exit_code;
  $sub_form_state['values']['compile_geshi_start_time'] = $project->last_start_time;
  $sub_form_state['values']['compile_geshi_end_time'] = $project->last_end_time;

  // FORMAT
  $sub_form_state['values']['format'] = NULL;

  // PROJECT TERM
  $project_vid = variable_get('compile_geshi_projects_vid', 0);
  if ($project_vid != 0) {
    $vocabulary = taxonomy_vocabulary_load($project_vid);
    $tid = $project->project_tid;
    if ($vocabulary->tags) {
      // tags are a special case, also you cannot choose tags for a forum...
      $sub_form_state['values']['taxonomy']['tags'][$project_vid] = taxonomy_implode_tags($project->project_tid, $project_vid);
    }
    elseif ($vocabulary->multiple) {
      // forums should never use multiple either
      $sub_form_state['values']['taxonomy'][$project_vid][$project->project_tid] = $project->project_tid;
    }
    else {
      $sub_form_state['values']['taxonomy'][$project_vid] = $project->project_tid;
    }
  }

  // LOG
  $sub_form_state['values']['log'] = t('Result node automatically created by Compile GeSHi');

  // OPERATION
  $sub_form_state['values']['op'] = t('Save');

  // STATUS
  $sub_form_state['values']['status'] = $project->new_results_status;

  // Values that will get proper defaults automatically
  //$sub_form_state['values']['date'] = date('Y-m-d H:i:s O', time());

  // COMPILE GESHI RESULT POST
  $compile_result = (object)array(
    'type' => 'compile_geshi_result',
    //'uid' => ..., // defaults to 0 -- setting uid here only works for the admin
  );

  // IMPORTANT NOTE:
  // This call generates a form for the current user, not the future user of
  // the new compile result node. This is an important distinction since some
  // parameters just cannot be passed to this function if we want it to work.
  // For instance, the format is set to default so it works. Any other format
  // could fail and the creation of the compile result would fail. ("default"
  // is available to all users!)

  drupal_execute('compile_geshi_result_node_form', $sub_form_state, $compile_result);
  $compile_result->nid = $sub_form_state['nid'];

  if (!$compile_result->nid) {
    // it did not work... hmmm...
    drupal_set_message(t('Compile GeSHi result node could not be created for the last compilation of project @name.', array('@name' => $project->name)), 'error');
    watchdog(
      'compile_geshi',
      'Compile GeSHi result node could not be created for the last compilation of project @name.',
      array('@name' => $project->name),
      WATCHDOG_WARNING,
      'taxonomy/term/' . $project->project_tid);
    return;
  }

  // Now we can save the author in the new post as expected by the
  // administrator; when we try to set the UID in the node info
  // before calling drupal_execute() it is reset for security reason
  // and the result is that the UID is set to 0 (anonymous)
  $sql = "UPDATE {node} SET uid = %d WHERE nid = %d";
  db_query($sql, $project->uid, $compile_result->nid);

  $sql = "UPDATE {node_revisions} SET format = %d"
          . " WHERE nid = %d"
            . " AND vid = (SELECT vid FROM {node} WHERE nid = %d)";
  db_query($sql, $project->format, $compile_result->nid, $compile_result->nid);

  // if you have boost, we want to reset the GeSHi nodes that were
  // just compiled so the new results show up on next load
  if (function_exists('boost_expire_node')) {
    $sql = "SELECT n.nid FROM {node} n, {term_node} t WHERE n.nid = t.nid AND n.vid = t.vid AND t.tid = %d";
    $result = db_query($sql, $project->project_tid);
    while ($row = db_fetch_array($result)) {
      $node = node_load(array('nid' => $row['nid']));
      if ($node) {
        boost_expire_node($node);
      }
      // this count as one because it is pretty heavy
      --$max_count;
    }
    if ($max_count < 0) {
      $max_count = 0;
    }
  }
}

/**
 * Retrieve the GeSHi nodes of projects that are active and
 * of which nodes have changed.
 *
 * Note: this function assumes that you do not have thousands of
 *       projects each with thousands of nodes... otherwise, it
 *       is likely that the SQL will be way too slow.
 */
function _compile_geshi_extract_source_nodes(&$max_count, $start_compiling) {
  // search for projects ordered from the oldest node to the newest.
  // we only consider active projects and to (supposedly) accelerate the SQL
  // we only consider projects that are marked as modified
  $sql_projects = "SELECT p.project_tid, p.path, p.version, p.build, p.command_line, p.force_filename_lowercase, t.name"
    . " FROM {node} n, {term_node} tn, {term_data} t, {compile_geshi_project} p, {compile_geshi_node} g"
    . " WHERE n.type = 'geshinode' AND n.status = 1 AND tn.nid = n.nid AND tn.vid = n.vid"
      . " AND tn.tid = p.project_tid AND p.status = 1 AND p.compiling = 0 AND p.modified = 1"
      . " AND g.geshi_nid = n.nid AND g.last_extraction < n.changed AND t.tid = p.project_tid AND t.vid = %d"
    . " GROUP BY p.project_tid, p.path, p.version, p.build, p.command_line, p.force_filename_lowercase, t.name, n.changed"
    . " ORDER BY n.changed DESC";
  $sql_nodes = "SELECT n.nid, n.title, n.changed, r.body, gft.extension"
    . " FROM {node} n, {node_revisions} r, {term_node} t, {term_node} ft, {compile_geshi_file_type} gft"
    . " WHERE n.type = 'geshinode' AND n.status = 1 AND r.nid = n.nid AND r.vid = n.vid"
      . " AND t.nid = n.nid AND t.vid = n.vid AND t.tid = %d"
      . " AND ft.nid = n.nid AND ft.vid = n.vid AND gft.file_type_tid = ft.tid";
  $result = db_query($sql_projects, variable_get('compile_geshi_projects_vid', 0));
  while ($max_count > 0 && ($project = db_fetch_object($result))) {
    // search for the nodes in this project that need extraction
    $project_path = _compile_geshi_project_path($project);
    $now = time();
    $nodes = db_query($sql_nodes, $project->project_tid);
    $errcnt = 0;
    while ($max_count > 0 && ($geshi_node = db_fetch_object($nodes))) {
      --$max_count;
      if (!is_dir($project_path)) {
        $max_count = max(--$max_count, 0);
        mkdir($project_path);
        if (!is_dir($project_path)) {
          // we cannot create the project sub-folder, err and mark the project as inactive to
          // avoid wasting time on it later
          watchdog(
            'compile_geshi',
            'Cannot compile GeSHi project "@name" since creating folder "@path" failed. The project was disabled for now.',
            array('@name' => $project->name, '@path' => $project_path),
            WATCHDOG_CRITICAL,
            'admin/content/taxonomy/edit/term/' . $project->project_tid);
          $sql = "UPDATE {compile_geshi_project} SET active = 0 WHERE project_tid = %d";
          db_query($sql, $project->tid);
          ++$errcnt;
          break;
        }
      }
      $filename = $geshi_node->title;
      if ($project->force_filename_lowercase) {
        $filename = drupal_strtolower($filename);
      }
      $extension = $geshi_node->extension;
      if ($extension && $extension[0] != '.') {
        // add a period unless the extension is empty (which is valid for scripts, programs, etc. in Unix)
        $extension = '.' . $extension;
      }
      $l = strlen($extension);
      if ($l && substr($filename, -$l) != $extension) {
        $filename .= $extension;
      }
      elseif ($l == strlen($filename)) {
        // this means the filename is just the extension which is not acceptable
        // (note that if $l == 0 then we don't enter here because strlen() > 0 is always true here)
        $errmsg = 'The name (title) of GeSHi node "@name" cannot just be the file type extension.';
        watchdog(
          'compile_geshi',
          $errmsg,
          array('@name' => $filename),
          WATCHDOG_ERROR,
          'node/' . $geshi_node->nid);
        // skip this node...
        $sql = "UPDATE {node} SET extraction_error = '%s' WHERE nid = %d";
        db_query($sql, t($errmsg, array('@name' => $filename)), $geshi_node->nid);
        ++$errcnt;
        continue;
      }
      if (strpos($filename, '/') !== FALSE || strpos($filename, '\\') !== FALSE) {
        // this means the filename is just the extension which is not acceptable
        $errmsg = 'The name (title) of GeSHi node "@name" cannot include a slash or backslash.';
        watchdog(
          'compile_geshi',
          $errmsg,
          array('@name' => $filename),
          WATCHDOG_ERROR,
          'node/' . $geshi_node->nid);
        // skip this node...
        $sql = "UPDATE {node} SET extraction_error = '%s' WHERE nid = %d";
        db_query($sql, t($errmsg, array('@name' => $filename)), $geshi_node->nid);
        ++$errcnt;
        continue;
      }
      // clean up the filename
      // TODO: we should make sure that two nodes don't end up with the same name
      $filename = preg_replace('/\s/', '_', $filename);
      $output_file = $project_path . '/' . $filename;
      file_put_contents($output_file, $geshi_node->body);

      // that file was extracted, so save the last extraction time in the database
      $sql = "UPDATE {compile_geshi_node} SET extraction_error = NULL, last_extraction = %d WHERE geshi_nid = %d";
      db_query($sql, $now, $geshi_node->nid);
    }
    // if the project was totally empty, then the $project_path would not be a directory (it should not be at least)
    if ($max_count > 0 && $errcnt == 0 && $start_compiling && is_dir($project_path)) {
      --$max_count;

      // we just worked on a project and there are still some cycles
      // available, so we want to start compiling that project now
      $script_path = drupal_get_path('module', 'compile_geshi') . '/scripts/';
      $cmd = $script_path . $project->command_line . ' ' . escapeshellarg(dirname($project_path)) . ' ' . escapeshellarg(basename($project_path));
      watchdog(
        'compile_geshi',
        'Started command @cmd.',
        array('@cmd' => $cmd),
        WATCHDOG_WARNING,
        'taxonomy/term/' . $project->project_tid);
      pclose(popen($cmd, 'r')); // start the command line in the background

      // okay, this project is in compiling mode now
      $sql = "UPDATE {compile_geshi_project} SET compiling = 1, modified = 0, last_start_time = %d, last_end_time = 0 WHERE project_tid = %d";
      db_query($sql, time(), $project->project_tid);
    }
  }
}

// vim: ts=2 sw=2 et syntax=php
