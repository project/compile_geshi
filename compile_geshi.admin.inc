<?php

/**
 * @file
 * Setup menu and forms for the compile_geshi module.
 */

/**
 * Actual implementation of hook_menu().
 *
 * This function generates the array of menu items.
 */
function compile_geshi_menu_array() {
  $menu = array();

  // main settings for the compiler
  $menu['admin/settings/geshifilter/compile_geshi'] = array(
    'title' => 'GeSHi Compiler',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Compile code written in GeSHi nodes and report the results.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('compile_geshi_settings_form'),
    'access arguments' => array('administer compile_geshi'),
    'file' => 'compile_geshi.admin.inc',
  );

  return $menu;
}

/**
 * Form to setup the global settings.
 */
function compile_geshi_settings_form() {
  $form['compile_geshi_date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#description' => t('Format used to display compilation dates. This uses the PHP date() function format. The default is "Y/m/d H:i:s", without the quotes.'),
    '#default_value' => variable_get('compile_geshi_date_format', 'Y/m/d H:i:s'),
  );

  $form['compile_geshi_minimum_wait_between_compiles'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum wait between compiles'),
    '#description' => t('Number of seconds to wait before initiating a new compilation of a project. This prevents fast modifications from generating many useless compiles. The default is 1h (3600).'),
    '#default_value' => variable_get('compile_geshi_minimum_wait_between_compiles', 3600),
  );

  $form['compile_geshi_geshi_node_info_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Position of the Compilation info on GeSHi Nodes'),
    '#description' => t('Defines the weight of the compilation information on GeSHi nodes. The default is 10 which generally moves them at the bottom of the page.'),
    '#default_value' => variable_get('compile_geshi_geshi_node_info_weight', 10),
  );

  return system_settings_form($form);
}

/**
 * The actual implementation of the compile_geshi_FORM_ID_alter() for terms.
 */
function _compile_geshi_taxonomy_term_alter(&$form) {
  // make sure we are editing the Compilge GeSHi Project
  if ($form['vid']['#value'] == variable_get('compile_geshi_projects_vid', 0)) {
    if (isset($form['tid']['#value'])) {
      $sql = "SELECT * FROM {compile_geshi_project} WHERE project_tid = %d";
      $result = db_query($sql, $form['tid']['#value']);
      $project = db_fetch_object($result);
    }
    if (empty($project)) {
      $project = (object)array(
        'project_tid' => 0,
        'uid' => 1,
        'format' => 0,
        'command_line' => '',
        'force_filename_lowercase' => FALSE,
        'version' => '1.0',
        'build' => 1,
        'result_count' => 3,
        'new_results_status' => 1,
        'tarball' => -1,
        'path' => '',
        'status' => 1,
        // Not editable
        //'compiling' => 0,
        //'last_exit_code' => 0,
        //'last_start_time' => 0,
        //'last_end_time' => 0,
        //'modified' => 1,
      );
    }

    $form['compile_geshi_project'] = array(
      '#type' => 'fieldset',
      '#title' => t('Compile GeSHi Project'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['compile_geshi_project']['compile_geshi_project_status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#description' => t('Whether this project is currently enabled and processed as required.'),
      '#default_value' => $project->status,
    );

    $form['compile_geshi_project']['compile_geshi_project_uid'] = array(
      '#type' => 'textfield',
      '#title' => t('Compile GeSHi Results Author'),
      '#description' => t('Compile results will be created using this user as the author.'),
      '#default_value' => $project->uid,
    );

    $formats = filter_formats();
    $options = array();
    foreach ($formats as $f) {
      $options[] = $f->name;
    }
    $form['compile_geshi_project']['compile_geshi_project_format'] = array(
      '#type' => 'select',
      '#title' => t('Compile GeSHi Results Node Body Format'),
      '#description' => t('Define the format to use for the Body of the Compile GeSHi Results nodes. This format may make use of the Compile GeSHi filter and can also be protective of unwanted edits.'),
      '#default_value' => $project->format,
      '#options' => $options,
    );

    $script_path = drupal_get_path('module', 'compile_geshi') . '/scripts/';
    $files = file_scan_directory($script_path, '.*', array('.', '..', '.svn', 'CVS', '.htaccess', 'index.html'), 0, FALSE, 'basename');
    $scripts = array();
    foreach ($files as $f) {
      $scripts[$f->basename] = $f->basename;
    }
    $form['compile_geshi_project']['compile_geshi_project_command_line'] = array(
      '#type' => 'select',
      '#title' => t('Command Line'),
      '#description' => t('The command line that will be executed to run the compilation process in the source folder of this project. You may create and install new scripts under compile_geshi/scripts.'),
      '#default_value' => $project->command_line,
      '#options' => $scripts,
    );

    $form['compile_geshi_project']['compile_geshi_project_force_filename_lowercase'] = array(
      '#type' => 'checkbox',
      '#title' => t('Force all filenames to lowercase'),
      '#description' => t('Some languages require that the name of the source file match the name of the program, library, package. This feature let you use capitals for your titles and force them to lowercase on your file system.'),
      '#default_value' => $project->force_filename_lowercase,
    );

    $form['compile_geshi_project']['compile_geshi_project_version'] = array(
      '#type' => 'textfield',
      '#title' => t('Project version'),
      '#description' => t('The version of this project. You may change this number at any time. It will be effective immediately.'),
      '#default_value' => $project->version,
    );

    $form['compile_geshi_project']['compile_geshi_project_build'] = array(
      '#type' => 'textfield',
      '#title' => t('Project build'),
      '#description' => t('The build counter starts at 1 and is automatically incremented on each compile of this project. You may change this number at any time. It will be effective immediately.'),
      '#default_value' => $project->build,
    );

    $form['compile_geshi_project']['compile_geshi_project_result_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of results pages to keep'),
      '#description' => t('Each project may keep Compile GeSHi Results pages for record. The last page comes from the latest compilation and includes the output of the compiler and a few things from Compile GeSHi scripts. Use 0 to keep all the results. Use a positive number to keep that many results (default 3). Use -1 to turn off this feature.'),
      '#default_value' => $project->result_count,
    );

    $form['compile_geshi_project']['compile_geshi_project_new_results_status'] = array(
      '#type' => 'radios',
      '#title' => t('Status of new Compile GeSHi Results nodes'),
      '#description' => t('When creating a new Compile GeSHi Result node, use this status (Published or not) to create the node.'),
      '#default_value' => $project->new_results_status,
      '#options' => array('1' => 'Published', '0' => 'Unpublished'),
    );

    $form['compile_geshi_project']['compile_geshi_project_tarball'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of tarballs to keep'),
      '#description' => t('The source can automatically be saved in a compressed tarball. This option allows you to choose how many of those tarballs should be kept (-1 - none, 0 - all, a positive number of that many)'),
      '#default_value' => $project->tarball,
    );

    $form['compile_geshi_project']['compile_geshi_project_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to source folders'),
      '#description' => t('This is a full path to a folder that is accessible to your webserver. The server will create sub-folders where the GeSHi Source node content is to be saved. It will also include the latest results of compilation and have a sub-folder for tarballs.'),
      '#default_value' => $project->path,
    );

    $form['#submit'][] = '_compile_geshi_project_term_form_submit';
  }
  elseif ($form['vid']['#value'] == variable_get('compile_geshi_file_type_vid', 0)) {
    if (isset($form['tid']['#value'])) {
      $sql = "SELECT * FROM {compile_geshi_file_type} WHERE file_type_tid = %d";
      $result = db_query($sql, $form['tid']['#value']);
      $file_type = db_fetch_object($result);
    }
    if (empty($file_type)) {
      $file_type = (object)array(
        'file_type_tid' => 0,
        'status' => 1,
        'extension' => '',
      );
    }

    $form['compile_geshi_file_type'] = array(
      '#type' => 'fieldset',
      '#title' => t('Compile GeSHi File Type'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['compile_geshi_file_type']['compile_geshi_file_type_status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#description' => t('Whether the files in this category are to be saved in the source folder.'),
      '#default_value' => $file_type->status,
    );

    $form['compile_geshi_file_type']['compile_geshi_file_type_extension'] = array(
      '#type' => 'textfield',
      '#title' => t('File type extension'),
      '#description' => t('The file extension used with this file type. (typical extensions are .c, .cxx, .ads, .adb, .f77, etc.) An empty extension is a valid choice. The period is not required.'),
      '#default_value' => $file_type->extension,
    );

    $form['#submit'][] = '_compile_geshi_file_type_term_form_submit';
  }
}

/**
 * Save the project data entered by the user.
 */
function _compile_geshi_project_term_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $project_tid = $form_state['values']['tid'];

  $sql = "UPDATE {compile_geshi_project}"
    . " SET status = %d, uid = %d, format = %d, command_line = '%s',"
      . " force_filename_lowercase = %d, version = '%s', build = %d,"
      . " result_count = %d, new_results_status = %d, tarball = %d,"
      . " path = '%s'"
    . " WHERE project_tid = %d";
  db_query($sql, $values['compile_geshi_project_status'], $values['compile_geshi_project_uid'],
    $values['compile_geshi_project_format'], $values['compile_geshi_project_command_line'],
    $values['compile_geshi_project_force_filename_lowercase'],
    $values['compile_geshi_project_version'], $values['compile_geshi_project_build'],
    $values['compile_geshi_project_result_count'], $values['compile_geshi_project_new_results_status'],
    $values['compile_geshi_project_tarball'], $values['compile_geshi_project_path'],
    $project_tid);

  if (db_affected_rows() == 0) {
    $sql = "INSERT INTO {compile_geshi_project}"
      . " (project_tid, status, uid, format, command_line, force_filename_lowercase, version, build,"
        . " result_count, new_results_status, tarball, path)"
      . " VALUES (%d, %d, %d, %d, '%s', %d, '%s', %d, %d, %d, %d, '%s')";
    db_query($sql, $project_tid, $values['compile_geshi_project_status'], $values['compile_geshi_project_uid'],
      $values['compile_geshi_project_format'], $values['compile_geshi_project_command_line'],
      $values['compile_geshi_project_force_filename_lowercase'],
      $values['compile_geshi_project_version'], $values['compile_geshi_project_build'],
      $values['compile_geshi_project_result_count'], $values['compile_geshi_project_new_results_status'],
      $values['compile_geshi_project_tarball'], $values['compile_geshi_project_path']);
  }
}

/**
 * Save the file type data entered by the user.
 */
function _compile_geshi_file_type_term_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $file_type_tid = $form_state['values']['tid'];

  $sql = "UPDATE {compile_geshi_file_type}"
    . " SET status = %d, extension = '%s'"
    . " WHERE file_type_tid = %d";
  db_query($sql, $values['compile_geshi_file_type_status'], $values['compile_geshi_file_type_extension'],
    $file_type_tid);

  if (db_affected_rows() == 0) {
    $sql = "INSERT INTO {compile_geshi_file_type}"
      . " (file_type_tid, status, extension)"
      . " VALUES (%d, %d, '%s')";
    db_query($sql, $file_type_tid, $values['compile_geshi_file_type_status'],
      $values['compile_geshi_file_type_extension']);
  }
}

// vim: ts=2 sw=2 et syntax=php
