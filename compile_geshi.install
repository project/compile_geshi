<?php

/**
 * @file compile_geshi.install
 *
 * Create the compile geshi tables
 */

/**
 * Implementation of hook_schema().
 */
function compile_geshi_schema() {
  $schema = array();

  $schema['compile_geshi_project'] = array(
    'fields' => array(
      'project_tid' => array(
        'description' => 'The project term.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'The current status of this project (0 - off, 1 - active).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'uid' => array(
        'description' => 'The author of the result nodes. By default use Admin (UID=1)',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'format' => array(
        'description' => 'Format of the result nodes that transforms file names and line numbers into links.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'command_line' => array(
        'description' => 'The command line to run the compiler. In general, the compilation command will be wrapped in a script that generates the compilation start and end dates with "date +%s" and handle the compile output as required.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'force_filename_lowercase' => array(
        'description' => 'Whether the filenames must be forced to lowercase before being used. This is not applied to the user entered extension.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => FALSE,
      ),
      'version' => array(
        'description' => 'The version of this project (1.0, 2-4.2, 3:6.2a, etc.).',
        'type' => 'text',
        'not null' => TRUE,
        'default' => '1.0',
      ),
      'build' => array(
        'description' => 'The build counter of this project. The counter is increased once a compilation succeeded (which does not mean that there was no error! Just that we got results back.).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'result_count' => array(
        'description' => 'The number of build results to keep. Use 0 to keep them all. This applies to the compile_geshi_results nodes. -1 means do not create result nodes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'new_results_status' => array(
        'description' => 'The value of the result node "status" when creating them (i.e. whether result nodes are created published or unpublished.)',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'tarball' => array(
        'description' => 'Whether a tarball of the source files should be generated for each build. 0 means off, -1 means create and keep them all, 1 or more means create a tarball and keep that many versions.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => -1,
      ),
      'path' => array(
        'description' => 'The path (preferably a full path) to your source environment. Note that this is expected to be outside of the web server environment, but your web server needs write permission to create folders and files.',
        'type' => 'text',
        'not null' => TRUE,
        'default' => 'active',
      ),
      'compiling' => array(
        'description' => 'Whether we\'re compiling the current project (0 - not compiling, 1 - compiling).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'last_exit_code' => array(
        'description' => 'The exit code of the compiler command corresponding to the last compile.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'last_start_time' => array(
        'description' => 'Last start time the compiler was run against this node.',
        'type' => 'int', // should be a big int...
      ),
      'last_end_time' => array(
        'description' => 'Last end time the compiler was run against this node.',
        'type' => 'int', // should be a big int...
      ),
      'modified' => array(
        'description' => 'A flag to know whether a GeSHi node from this project was modified (saved) since the project was last compiled.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1, // by default mark as changed since we did not extract anything yet
      ),
    ),
    'indexes' => array(
      'last_run' => array('last_end_time'),
      'status' => array('status'),
      'compiling' => array('compiling'),
    ),
    'primary key' => array('project_tid'),
  );

  $schema['compile_geshi_results'] = array(
    'fields' => array(
      'result_nid' => array(
        'description' => 'The node with the output of the compiler command.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'version' => array(
        'description' => 'The version that was compiled and generated these results.',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'build' => array(
        'description' => 'The build that was compiled and generated these results.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'exit_code' => array(
        'description' => 'The exit code of the compiler command corresponding to that result.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'start_time' => array(
        'description' => 'Time when this compilation started.',
        'type' => 'int', // should be a big int...
        'not null' => TRUE,
      ),
      'end_time' => array(
        'description' => 'Time when this compilation ended.',
        'type' => 'int', // should be a big int...
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'exit_code' => array('exit_code'),
      'start_time' => array('start_time'),
    ),
    'primary key' => array('result_nid'),
  );

  $schema['compile_geshi_node'] = array(
    'fields' => array(
      'geshi_nid' => array(
        'description' => 'The GeSHi node we worked on.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      // IMPORTANT: The project_tid is used to know whether a GeSHi node is moved from project to project
      'project_tid' => array(
        'description' => 'Reference to the project this node is in. Important to know whether a GeSHi node is moved from one project to another.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'extraction_error' => array(
        'description' => 'The extraction of this node failed and this field includes the error.',
        'type' => 'text',
      ),
      'last_extraction' => array(
        'description' => 'Last time the GeSHi source was extracted.',
        'type' => 'int', // should be a big int...
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'project_tid' => array('project_tid'),
      'last_extraction' => array('last_extraction'),
    ),
    'primary key' => array('geshi_nid'),
  );

  $schema['compile_geshi_file_type'] = array(
    'fields' => array(
      'file_type_tid' => array(
        'description' => 'The vocabulary term representing this file type.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Whether this file type is part of the source code (i.e. 1 - necessary for compilation, 0 - not necessary)',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'extension' => array(
        'description' => 'The expected extension for this file type; i.e. ".c", ".ads", ".f77", etc.',
        'type' => 'text',
      ),
    ),
    'primary key' => array('file_type_tid'),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function compile_geshi_install() {
  drupal_install_schema('compile_geshi');
}

/**
 * Implementation of hook_uninstall().
 */
function compile_geshi_uninstall() {
  drupal_uninstall_schema('compile_geshi');
  db_query("DELETE FROM {variable} WHERE name LIKE 'compile_geshi_%%'");
}

/**
 * Implementation of hook_enable().
 */
function compile_geshi_enable() {
  // first create a vocabulary for projects
  $vocabulary = db_fetch_array(db_query("SELECT * FROM {vocabulary} WHERE name = '%s'", t('Compile GeSHi Project')));
  if (!$vocabulary) {
    $vocabulary = array(
      'name' => t('Compile GeSHi Project'),
      'description' => t('Vocabulary of terms used to group your source files in different projects.'),
      'help' => t('Select the project this GeSHi node is part of. To create a new project, go to Admin » Content » Taxonomy'),
      'relations' => 1,
      'hierarchy' => 1,
      'multiple' => 0, // this could be 1 although at this time we do not support it
      'required' => 1,
      'tags' => 0,
      'module' => 'compile_geshi',
      'weight' => 5,
    );
  }
  // force this vocabulary to the geshinode every time
  $vocabulary['nodes']['geshinode'] = 1;
  taxonomy_save_vocabulary($vocabulary);
  variable_set('compile_geshi_projects_vid', $vocabulary['vid']);

  // now create another vocabulary for file types
  $vocabulary = db_fetch_array(db_query("SELECT * FROM {vocabulary} WHERE name = '%s'", t('Compile GeSHi File Type')));
  if (!$vocabulary) {
    $vocabulary = array(
      'name' => t('Compile GeSHi File Type'),
      'description' => t('Vocabulary of terms used to define the type of each GeSHi node file.'),
      'help' => t('Select the file type of this GeSHi node. Define types for your projects in Admin » Content » Taxonomy'),
      'relations' => 1,
      'hierarchy' => 1,
      'multiple' => 0,
      'required' => 1,
      'tags' => 0,
      'module' => 'compile_geshi',
      'weight' => -10, // must appear first for autopath to work right
    );
  }
  // force this vocabulary to the geshinode every time
  $vocabulary['nodes']['geshinode'] = 1;
  taxonomy_save_vocabulary($vocabulary);
  variable_set('compile_geshi_file_type_vid', $vocabulary['vid']);
}



// vim: ts=2 sw=2 et syntax=php
