<?php

/**
 * @file compile_geshi-geshi_node_info
 * Default theme implementation to display information about a GeSHi node.
 *
 * Available variables:
 * - $node -- the GeSHi node including $node->compile_geshi_last_extraction
 * - $project -- the corresponding Compile GeSHi project including:
 *   + $project->name -- name of the corresponding term
 *   + $project->description -- description of the corresponding term
 *   + $project->date_format -- the format to use to display a project date
 *   + $project->last_results -- a copy of the last compile_geshi_results for this project
 */
?>
<table>
  <tbody>
    <tr>
      <?php
        $name = '';
        if ($project->status == 0) {
          $name .= t('Inactive ');
          $class = 'compile-geshi-inactive';
        }
        else {
          $class = '';
        }
        $name .= $project->name;
        if ($project->version) {
          $name .= ' v' . $project->version;
          if ($project->build) {
            $name .= '-' . $project->build;
          }
        }
        $name .= ' (' . t('Project id #') . $project->project_tid . ')';
      ?>
      <td class="compile-geshi-geshi-node-label<?php echo $class ?>"><?php echo t('Project') ?></td>
      <td class="compile-geshi-geshi-node-value<?php echo $class ?>"><?php echo $name ?></td>
    </tr>
    <tr>
      <?php
        if ($project->compiling) {
          $process = t('Compiling') . ' (Started on ' . date($project->date_format, $project->last_start_time) . ')';
        }
        else {
          if ($project->modified) {
            $process = t('Ready for update');
          }
          elseif ($project->last_end_time) {
            $process = t('Done');
          }
          else {
            $process = t('Stand-by');
          }
          if ($project->last_end_time) {
            $end_date = ' (' . t('Last compiled on ') . date($project->date_format, $project->last_end_time) . ')';
          }
          else {
            $end_date = ' (' . t('Never compiled') . ')';
          }
          $process .= $end_date;
        }
      ?>
      <td class="compile-geshi-geshi-node-label<?php echo $class ?>"><?php echo t('Process') ?></td>
      <td class="compile-geshi-geshi-node-value<?php echo $class ?>"><?php echo $process ?></td>
    </tr>
    <?php
      if (isset($project->last_results->exit_code)) {
        $exit_code = $project->last_results->exit_code;
        if ($project->last_results->end_time) {
          $exit_code .= ' (Result received on ' . date($project->date_format, $project->last_results->end_time) . ')';
        }
    ?>
      <tr>
        <td class="compile-geshi-geshi-node-label<?php echo $class ?>"><?php echo t('Last Exit Code') ?></td>
        <td class="compile-geshi-geshi-node-value<?php echo $class ?>"><?php echo $exit_code ?></td>
      </tr>
    <?php
      }
    ?>
    <?php
      if (isset($node->compile_geshi_extraction_error)) {
    ?>
      <tr>
        <td class="compile-geshi-geshi-node-label<?php echo $class ?>"><?php echo t('Last Extract Error') ?></td>
        <td class="compile-geshi-geshi-node-value<?php echo $class ?>"><?php echo $node->compile_geshi_extraction_error ?></td>
      </tr>
    <?php
      }
    ?>
    <?php
      if ($project->description) {
    ?>
    <tr>
      <td class="compile-geshi-geshi-node-label<?php echo $class ?>"><?php echo t('Description') ?></td>
      <td class="compile-geshi-geshi-node-value<?php echo $class ?>"><?php echo $project->description ?></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>
<?php
// vim: ts=2 sw=2 et syntax=php
?>
